package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"

	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"

	"gitlab.com/zendrulat123/godash/go/cmd"
	routes "gitlab.com/zendrulat123/godash/go/routing"
	Renderer "gitlab.com/zendrulat123/godash/go/routing/templatemethods"
)

func main() {
	cmd.Execute()
	e := echo.New()
	e.Renderer = Renderer.Rend()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))
	routes.Routes(e)

	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	viper.SetConfigName("con") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	file := viper.GetString("env.file")
	times := viper.GetString("env.time")
	fmt.Println(file, times)
	e.Static("/static", "static")

	e.Logger.Fatal(e.Start(":1323"))

}
