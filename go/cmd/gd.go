package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// gdCmd represents the gd command
var gdCmd = &cobra.Command{
	Use:   "gd",
	Short: "A brief description of your command",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("gd called")

	},
}

func init() {
	rootCmd.AddCommand(gdCmd)

}
