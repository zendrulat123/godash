package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/spf13/viper"
)

type Data struct {
	Name   string
	Status string
}

var D Data

//get handler
func Dashboardapi(w http.ResponseWriter, r *http.Request) {

	//checking header
	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println("Post is chosen")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000/dashboard")
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000/dashboard")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	switch r.Method {

	case "GET":
		fmt.Println(r.Header.Get("Origin"))
		viper.SetConfigName("con") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("./config/") // config file path
		viper.AutomaticEnv()             // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		status := viper.GetString("env.status")
		name := viper.GetString("env.name")
		D = Data{Name: name, Status: status}
		fmt.Println(D)
		json.NewEncoder(w).Encode(D)
		D = Data{Name: "", Status: ""}

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")

	}
}
