package user

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	conn "gitlab.com/zendrulat123/godash/go/handler/db"
)

type User struct {
	ID           string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username     string `param:"username" query:"username" form:"username" json:"username" xml:"username"`
	Fullname     string `param:"fullname" query:"fullname" form:"fullname" json:"fullname" xml:"fullname"`
	Email        string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Passwordhash string `param:"passwordhash" query:"passwordhash" form:"passwordhash" json:"passwordhash" xml:"passwordhash"`
	Passwordraw  string `param:"passwordraw" query:"passwordraw" form:"passwordraw" json:"passwordraw" xml:"passwordraw"`
	Isdisabled   bool   `param:"isdisabled" query:"isdisabled" form:"isdisabled" json:"isdisabled" xml:"isdisabled"`
	SessionKey   string `param:"sessionkey" query:"sessionkey" form:"sessionkey" json:"sessionkey" xml:"sessionkey"`
}

// e.GET("/users/:id", getUser)
func GetUser(c echo.Context) error {
	// User ID from path `users/:id`..
	id := c.Param("id")
	fmt.Println(id)
	users := conn.GetAllDataUser()
	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	c.Response().WriteHeader(http.StatusOK)

	return json.NewEncoder(c.Response()).Encode(users[0])
}

func Userform(c echo.Context) error {
	return c.Render(http.StatusOK, "form.html", map[string]interface{}{})
}
