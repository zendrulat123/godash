package user

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"log"

	"github.com/labstack/echo/v4"
	conn "gitlab.com/zendrulat123/godash/go/handler/db"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID           string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username     string `param:"username" query:"username" form:"username" json:"username" xml:"username"`
	Fullname     string `param:"fullname" query:"fullname" form:"fullname" json:"fullname" xml:"fullname"`
	Email        string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Passwordhash string `param:"passwordhash" query:"passwordhash" form:"passwordhash" json:"passwordhash" xml:"passwordhash"`
	Passwordraw  string `param:"passwordraw" query:"passwordraw" form:"passwordraw" json:"passwordraw" xml:"passwordraw"`
	Isdisabled   bool   `param:"isdisabled" query:"isdisabled" form:"isdisabled" json:"isdisabled" xml:"isdisabled"`
	SessionKey   string `param:"sessionkey" query:"sessionkey" form:"sessionkey" json:"sessionkey" xml:"sessionkey"`
}

func Processuser(c echo.Context) error {

	username := c.FormValue("username")
	password := c.FormValue("password")
	email := c.FormValue("email")
	var id string
	if username != "" {
		data := conn.Conn()
		stmt, err := data.Prepare("SELECT id, username, passwordraw FROM users WHERE username=?")
		if err != nil {
			log.Fatal(err)
		}
		defer stmt.Close()
		err = stmt.QueryRow(username).Scan(&id, &username, &password)
		switch err {
		case sql.ErrNoRows:
			fmt.Println("No rows were returned!")
			// close db when not in use
			defer data.Close()
			return c.Render(http.StatusOK, "form.html", map[string]interface{}{})
		case nil:
			fmt.Println(password, username)
			// close db when not in use
			defer data.Close()
			return c.Render(http.StatusOK, "welcome.html", map[string]interface{}{
				"userid":   id,
				"username": username,
				"email":    email,
			})
		default:
			panic(err)
		}
	} else {
		// close db when not in use

		fmt.Println("no user was found")
		return c.Render(http.StatusOK, "form.html", map[string]interface{}{})

	}
}

func CreateUser(c echo.Context) error {

	//get password/username
	username := c.FormValue("username")
	password := c.FormValue("password")
	email := c.FormValue("email")
	fullname := c.FormValue("fullname")

	//encrypt it
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
		if err != nil {
			fmt.Println(err, "Err: Password Encryption  failed")
		}
		json.NewEncoder(c.Response().Writer).Encode(err)
	}

	//encrypt it
	usernamehash, err := bcrypt.GenerateFromPassword([]byte(username), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
		if err != nil {
			fmt.Println(err, "Err: Password Encryption  failed")
		}
		json.NewEncoder(c.Response().Writer).Encode(err)
	}

	cookie := new(http.Cookie)
	cookie.Name = username
	cookie.Value = string(usernamehash)
	cookie.Expires = time.Now().Add(96 * time.Hour)
	c.SetCookie(cookie)

	//opening database
	data := conn.Conn()
	Pass := string(pass)
	isdisabled := true
	// query

	stmt, err := data.Prepare("INSERT INTO users(username, fullname, email, passwordhash, passwordraw, isdisabled, sessionkey) VALUES(?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	userstemp := User{Username: username, Fullname: fullname, Email: email, Passwordhash: Pass, Passwordraw: password, Isdisabled: isdisabled, SessionKey: string(usernamehash)}
	fmt.Println(userstemp)
	res, err := stmt.Exec(userstemp.Username, userstemp.Fullname, userstemp.Email, userstemp.Passwordhash, userstemp.Passwordraw, userstemp.Isdisabled, userstemp.SessionKey)
	if err != nil {
		log.Fatal(err)
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	fmt.Println("reached query")
	return c.Redirect(http.StatusFound, "/user/"+fmt.Sprint(lastId))
}
