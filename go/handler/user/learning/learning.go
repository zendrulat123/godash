package user

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Learning(c echo.Context) error {
	userids := c.Param("userid")

	return c.Render(http.StatusOK, "learning.html", map[string]interface{}{
		"userid": userids,
	})

}
