package postdashboard

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"gitlab.com/zendrulat123/godash/go/cmd/ut"
)

func Apiwatcher(c echo.Context) error {
	api := c.FormValue("api")
	fmt.Println(`curl -XGET -H "Content-type: application/json" '` + api + `'`)
	err, out, errout := ut.Shellout(`curl -v -XGET -w "%{http_code}" "Content-type: application/json" '` + api + `'`)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(out)
	fmt.Println("--- errs ---")
	fmt.Println(errout)
	viper.SetConfigName("con") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}

	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"path":    D.Path,
		"time":    D.Time,
		"name":    D.Name,
		"api":     out,
		"apistat": errout,
	})
}
