package postdashboard

import (
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"gitlab.com/zendrulat123/godash/go/cmd/ut"
)

type Data struct {
	Name string
	Time string
	Path string
}

var D Data

//updates through a POST the path/watch in the config
func Postwatcher(c echo.Context) error {
	path := c.FormValue("path")
	times := c.FormValue("time")
	viper.SetConfigName("con") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	viper.Set("env.path", path)
	viper.Set("env.time", times)
	name := viper.GetString("env.name")
	viper.WriteConfig()
	D = Data{Name: name, Path: path, Time: times}
	file := ut.Watch(path)
	ut.ConfigAddFile("config/con.yaml", "  file: "+"\""+file+"\"")

	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"path": D.Path,
		"time": D.Time,
		"name": D.Name,
		"file": file,
	})

}
