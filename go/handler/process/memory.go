package postdashboard

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"gitlab.com/zendrulat123/godash/go/cmd/ut"
)

func Memory(c echo.Context) error {
	//form data
	ml := c.FormValue("ml")
	git := c.FormValue("git")
	fmt.Println(ml, git)
	//create file
	f, m := CreateBase(ml, "mem.go")
	fmt.Println(f)
	//setup config
	viper.SetConfigName("con") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	//add props to config
	ut.ConfigAddFile("config/con.yaml", "  projdir: "+"\""+ml+"\"")
	ut.ConfigAddFile("config/con.yaml", "  git: "+"\""+git+"\"")

	//pull from config
	path := viper.GetString("env.path")
	time := viper.GetString("env.time")
	name := viper.GetString("env.name")
	api := viper.GetString("env.file")

	tm := template.Must(template.New("queue").Parse(memf))
	err = tm.Execute(f, D)
	if err != nil {
		log.Print("execute: ", err)
	}
	tmm := template.Must(template.New("queue").Parse(memf))
	err = tmm.Execute(m, D)
	if err != nil {
		log.Print("execute: ", err)
	}
	pleft := strings.Split(ml, "/")[1]
	//install dependencies in main.go
	err, out, errout := ut.Shellout("cd " + pleft + " && go mod init " + git + " && go mod tidy && go mod vendor")
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(out)
	fmt.Println("--- errs ---")
	fmt.Println(errout)
	defer f.Close()

	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"path": path,
		"time": time,
		"name": name,
		"api":  api,
	})
}

//creates the mem.go
func CreateBase(p string, filename string) (*os.File, *os.File) {
	if err := os.MkdirAll(p, os.ModeSticky|os.ModePerm); err != nil {
		fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
	} else {
		fmt.Println("Whoops, could not create directory(ies) because", err)
	}

	//making main.go
	tfile, err := os.Create(p + "mem.go")
	if isError(err) {
		fmt.Println("error -", err)
	}
	//making main.go
	mfile, err := os.Create(p + "main.go")
	if isError(err) {
		fmt.Println("error -", err)
	}

	return tfile, mfile

}
func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}

//used for updating config
func UpdateText(f string, o string, n string) {
	fmt.Println(f, o, n)
	input, err := ioutil.ReadFile(f)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	output := bytes.Replace(input, []byte(o), []byte(n), -1)

	fmt.Println("file: ", f, " old: ", o, " new: ", n)

	if err = ioutil.WriteFile(f, output, 0666); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

//mem.go file
var memf = `
package main
 
import (
	"log"
	"runtime"
	"time"
)
 
// Print system resource usage every 2 seconds.
func System() {
	mem := &runtime.MemStats{}
 
	for {
		cpu := runtime.NumCPU()
		log.Println("CPU:", cpu)
 
		rot := runtime.NumGoroutine()
		log.Println("Goroutine:", rot)
 
		// Byte
		runtime.ReadMemStats(mem)
		log.Println("Memory:", mem.Alloc)
 
		time.Sleep(2 * time.Second)
		log.Println("-------")
	}
}
  `

//main.go file
var mainT = `

package main

import(
  "fmt"

)

func main(){
	fmt.Println("works")
	System()
}
  `
