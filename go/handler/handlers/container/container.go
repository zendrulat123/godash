package container

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Container(c echo.Context) error {
	userids := c.Param("userid")

	return c.Render(http.StatusOK, "container.html", map[string]interface{}{
		"userid": userids,
	})

}
