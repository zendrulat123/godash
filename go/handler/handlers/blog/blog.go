package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Blog(c echo.Context) error {

	return c.Render(http.StatusOK, "blog.html", map[string]interface{}{})

}
