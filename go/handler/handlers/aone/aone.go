package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Aone(c echo.Context) error {

	return c.Render(http.StatusOK, "aone.html", map[string]interface{}{})

}
