package handlers

import (
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

type Data struct {
	Name string
	Path string
	Time string
}

var D Data

func Dashboard(c echo.Context) error {
	viper.SetConfigName("con") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	path := viper.GetString("env.path")
	name := viper.GetString("env.name")
	time := viper.GetString("env.time")
	D = Data{Name: name, Path: path, Time: time}
	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"name": D.Name,
		"path": D.Path,
		"time": D.Time,
	})
}
