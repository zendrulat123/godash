package db

import (
	"fmt"
	//conn "gitlab.com/zendrulat123/godash/go/handler/db"
)

type User struct {
	ID           string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Username     string `param:"username" query:"username" form:"username" json:"username" xml:"username"`
	Fullname     string `param:"fullname" query:"fullname" form:"fullname" json:"fullname" xml:"fullname"`
	PasswordHash string `param:"passwordhash" query:"passwordhash" form:"passwordhash" json:"passwordhash" xml:"passwordhash"`
	IsDisabled   bool   `param:"isdisabled" query:"isdisabled" form:"isdisabled" json:"isdisabled" xml:"isdisabled"`
	SessionKey   string `param:"sessionkey" query:"sessionkey" form:"sessionkey" json:"sessionkey" xml:"sessionkey"`
}

//CreateUser creates a Post
func GetAllDataUser() []User {
	//opening database
	data := Conn()

	var (
		id           string
		username     string
		fullname     string
		passwordhash string
		isdisabled   bool
		sessionkey   string
		user         []User
	)
	i := 0
	//get from database
	rows, err := data.Query("select id, username, fullname, passwordhash, isdisabled, sessionkey from users")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id, &username, &fullname, &passwordhash, &isdisabled, &sessionkey)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}
		u := User{ID: id, Username: username, Fullname: fullname, PasswordHash: passwordhash, IsDisabled: isdisabled, SessionKey: sessionkey}
		user = append(user, u)

	}
	defer rows.Close()
	defer data.Close()
	return user
}
