package routes

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	conn "gitlab.com/zendrulat123/godash/go/handler/db"
	"gitlab.com/zendrulat123/godash/go/handler/handlers"
	dash "gitlab.com/zendrulat123/godash/go/handler/handlers"
	aone "gitlab.com/zendrulat123/godash/go/handler/handlers/aone"
	blog "gitlab.com/zendrulat123/godash/go/handler/handlers/blog"
	container "gitlab.com/zendrulat123/godash/go/handler/handlers/container"
	home "gitlab.com/zendrulat123/godash/go/handler/handlers/home"
	postdashboard "gitlab.com/zendrulat123/godash/go/handler/process"
	"gitlab.com/zendrulat123/godash/go/handler/user"
	learn "gitlab.com/zendrulat123/godash/go/handler/user/learning"
	userprocess "gitlab.com/zendrulat123/godash/go/handler/user/process/create"
)

func Routes(e *echo.Echo) {

	g := e.Group("/users")
	g.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
		bools, err := Searchusers(username, password)
		if err != nil {
			fmt.Println(err, "something went wrong with middleware")
		}
		if bools {
			return true, nil
		} else {
			return false, nil
		}
	}))

	//processing user
	e.POST("/userprocess", userprocess.Processuser)
	e.POST("/usercreate", userprocess.CreateUser)
	//dashboard
	e.POST("/procwatch", postdashboard.Postwatcher)
	e.POST("/apiwatch", postdashboard.Apiwatcher)
	e.POST("/mem", postdashboard.Memory)

	//get dash
	e.GET("/dashboard", dash.Dashboard)
	e.GET("/user/:id", user.GetUser)
	e.GET("/learning/:userid", learn.Learning)
	e.GET("/blog", blog.Blog)
	e.GET("/aone", aone.Aone)
	e.GET("/container/:userid", container.Container)
	e.GET("/", home.Home)
	e.GET("/dash", handlers.Dashboard)

}

//used for authentication above
func Searchusers(username string, password string) (bool, error) {
	//opening database
	var err error
	//https://medium.com/@alok.sinha.nov/query-vs-exec-vs-prepare-in-golang-e7c49212c36c
	data := conn.Conn()
	if username != "" {
		stmt, err := data.Prepare("SELECT username, passwordraw FROM users WHERE username=?")
		if err != nil {
			log.Fatal(err)
		}
		defer stmt.Close()
		err = stmt.QueryRow(username).Scan(&username, &password)
		switch err {
		case sql.ErrNoRows:
			fmt.Println("No rows were returned!")
			// close db when not in use
			defer data.Close()
			return false, err
		case nil:
			fmt.Println(password, username)
			// close db when not in use
			defer data.Close()
			return true, err
		default:
			panic(err)
		}
	} else {
		// close db when not in use
		defer data.Close()
		fmt.Println("no user was found")
		return false, err
	}
}
